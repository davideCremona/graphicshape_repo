package GraphicShapes;

import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

/**
 * defines only a graphic shape.
 *
 */
public class NSidesPolygon extends Polygon {
	
	/** generated serial version UID */
	private static final long serialVersionUID = -4888498268428534838L;

	/** the points list */
	protected List<Point> points;
	
	/** the center of this graphic shape */
	protected Point center;
	
	/** the radius of this graphic shape */
	protected int radius;
	
	/** theta for this graphic shape */
	protected double theta;
	
	/** the sides of the son class */
	private int sonSides;
	
	public NSidesPolygon(int radius, Point center, int sides) {
		this.center = center;
		this.radius = radius;
		this.points = new ArrayList<Point>();
		this.sonSides = sides;
		this.setupTheta(sides);
		this.setupPoints();
	}
	
	/**
	 * sets up the points for a Graphic Shape
	 */
	protected void setupPoints(){
		for(int i=0; i<sonSides; i++){
			double x = this.getRadius()*Math.cos(i*theta);
			double y = this.getRadius()*Math.sin(i*theta);
			
			Point p = new Point((int)(this.center.getX() + x), (int)(this.center.getY() + y));
			
			this.addPoint((int)p.getX(), (int)p.getY());
			
			this.addPointToList(p);
		}
	}
	
	/**
	 * gets the radius of this shape
	 * @return the radius
	 */
	public int getRadius(){
		return this.radius;
	}
	
	/**
	 * gets the center of this shape
	 * @return the center.
	 */
	public Point getCenter(){
		return this.center;
	}
	
	/**
	 * add a point to the points list.
	 */
	protected void addPointToList(Point p){
		this.points.add(p);
	}
	
	/**
	 * gets the list of points of this shape
	 * @return the list of points.
	 */
	protected List<Point> getPoints(){
		return this.points;
	}
	
	protected void setupTheta(int sides){
		this.theta = 2*Math.PI/sides;
	}
}
